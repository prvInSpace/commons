package cymru.prv.commons.json

import org.json.JSONArray
import org.json.JSONObject

/**
 * Converts a list of JSON serializable objects
 * to a JSON array containing the converted objects
 */
fun List<JsonSerializable>.toJsonArray(): JSONArray {
    val array = JSONArray()
    this.forEach {
        array.put(it.toJson())
    }
    return array
}

fun List<JSONObject>.convertToJsonArray(): JSONArray {
    val array = JSONArray()
    this.forEach { array.put(it) }
    return array
}