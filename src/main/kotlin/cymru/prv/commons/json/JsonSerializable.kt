package cymru.prv.commons.json

import org.json.JSONObject

/**
 * A simple interface indicating that a object can be
 * converted to a JSON object
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
interface JsonSerializable {

    /**
     * Converts the object into a JSON object
     */
    fun toJson(): JSONObject

}